import styles from "./bottom-bar.module.css";
import previousParagraph from "./previousParagraph.svg";
import previousSentence from "./previousSentence.svg";
import play from "./play.svg";
import pause from "./pause.svg";
import skipParagraphImage from "./skipParagraph.svg";

interface BottomBarProps {
  isPlaying: boolean;
  togglePlaying: () => void;
  rewindParagraph: () => void;
  rewindSentence: () => void;
  skipParagraph: () => void;
}

export const BottomBar = ({
  isPlaying,
  rewindParagraph,
  rewindSentence,
  skipParagraph,
  togglePlaying,
}: BottomBarProps) => {
  return (
    <div className={styles.bottomBarWrapper}>
      <div className={styles.innerWrapper}>
        <button
          className={styles.previousParagraphButton}
          onClick={() => {
            rewindParagraph();
          }}
        >
          <img
            className={styles.basicButtonImage}
            src={previousParagraph}
            alt="previous paragraph"
          />
        </button>
        <button
          className={styles.previousSentenceButton}
          onClick={() => {
            rewindSentence();
          }}
        >
          <img
            className={styles.basicButtonImage}
            src={previousSentence}
            alt="previous sentence"
          />
        </button>
        <button
          className={styles.pausePlayButton}
          onClick={() => {
            togglePlaying();
          }}
        >
          {isPlaying ? (
            <img
              className={styles.pausePlayButtonImage}
              src={pause}
              alt="pause"
            />
          ) : (
            <img
              className={styles.pausePlayButtonImage}
              src={play}
              alt="play"
            />
          )}
        </button>
        <button
          className={styles.skipParagraphButton}
          onClick={() => {
            skipParagraph();
          }}
        >
          <img
            className={styles.basicButtonImage}
            src={skipParagraphImage}
            alt="skip paragraph"
          />
        </button>
      </div>
    </div>
  );
};
