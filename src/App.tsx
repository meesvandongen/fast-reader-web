import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import { Word } from "./Word";
import MenuIcon from "@material-ui/icons/Menu";
import { AddTextPage } from "./AddTextPage";
import { HomePage } from "./HomePage/HomePage";
import { BottomBar } from "./BottomBar/BottomBar";

const articleSampleText = `
Demonstraties moeten voorlopig niet verboden worden, die conclusie hebben de 25 burgemeesters in het Veiligheidsberaad maandag getrokken. Volgens voorzitter Hubert Bruls zou het volledig verbieden van alle demonstraties "amokmakers in de kaart spelen" en voor demonstranten het beeld bevestigen dat "hun vrijheid inderdaad wordt beknot".

Zondag mondden demonstraties tegen de coronamaatregelen in Amsterdam en Eindhoven uit in heftige rellen. Later op de dag werd het in meerdere steden onrustig uit protest tegen de avondklok.

Volgens Bruls moeten "echte demonstranten" de ruimte krijgen om hun geluid te laten horen. Wel zijn de burgemeesters binnen het Veiligheidsberaad het erover eens dat daar eisen aan moeten worden gesteld. "Bijvoorbeeld een maximumaantal mensen. En eigenlijk zou het alleen bij daglicht toegestaan moeten zijn", licht Bruls toe.

Ook minister Ferd Grapperhaus (Justitie en Veiligheid) vindt dat demonstraties nog toegestaan moeten blijven. "Ik denk dat het wel heel duidelijk is dat wat we zondag hebben gezien, niets met een demonstratie te maken heeft. Het is crimineel gedrag en het staat heel ver van demonstreren af."

'Politie kan bestrijding van rellen nog goed aan'
Volgens Bruls en Grapperhaus kan de politie het aanpakken van de rellen op dit moment nog goed aan. "Maar als het te lang duurt, moet je kijken wat de prioriteiten zijn en waar we wellicht hulp vandaan moeten halen", aldus Bruls.

Ook jongerenwerkers en straatcoaches kunnen volgens de Nijmeegse burgemeester helpen om nieuwe ongeregeldheden te voorkomen. "We kunnen hen ervan doordringen: doe geen gekke dingen."

Inzet van het leger om rellen te bestrijden is volgens Grapperhaus niet aan de orde. "Daar is in mijn aanwezigheid geen sprake van geweest."

`;

const Pages = { home: HomePage, text: AddTextPage };

export const App = () => {
  const [textParagraphs, setTextParagraphs] = useState(() => {
    return articleSampleText;
  });

  const [page, setPage] = useState("home");
  // @ts-ignore
  const Pagecomponent = Pages[page];

  return (
    <>
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">FastReader</Typography>
        </Toolbar>
      </AppBar>
      <Pagecomponent text={textParagraphs}> </Pagecomponent>
    </>
  );
};
