import React, { useCallback, useEffect, useState } from "react";
import { Word } from "../Word";
import { BottomBar } from "../BottomBar/BottomBar";
import Slider from "@material-ui/core/Slider";
import styles from "./home-page.module.css";

const lastWordOfSentenceDelayMultiplier = 4;
const lastSentenceOfParagaphDelayMultiplier = 8;

const convertWordsPerMinuteToDelay = (wordsPerMinute: number) => {
  const wordsPerSecond = wordsPerMinute / 60;
  const delay = 1 / wordsPerSecond;
  return delay * 1000;
};

const getIsLastWordOfSentence = (sentence: string[], wordIndex: number) => {
  return sentence.length - 1 === wordIndex;
};

const getIsLastSentenceOfParagaph = (
  paragraph: string[][],
  sentenceIndex: number
) => {
  return paragraph.length - 1 === sentenceIndex;
};

interface Pointer {
  paragraph: number;
  sentence: number;
  word: number;
}

interface HomePageprops {
  text: string;
}

export const HomePage = ({ text }: HomePageprops) => {
  const [textParagraphs, setTextParagraphs] = useState(() => {
    return text
      .split("\n")
      .filter((paragraph) => paragraph !== "")
      .map((paragraph) =>
        paragraph
          .split(".")
          .filter((sentence) => sentence !== "")
          .map((sentence) => sentence.split(" ").filter((word) => word !== ""))
      );
  });
  const [pointer, updatePointer] = useState<Pointer>({
    paragraph: 0,
    sentence: 0,
    word: 0,
  });

  const [wordsPerMinute, setWordsPerMinute] = useState(300);
  const [isPlaying, setIsPlaying] = useState(false);

  const getCurrentParagraph = useCallback(() => {
    return textParagraphs[pointer.paragraph];
  }, [pointer.paragraph, textParagraphs]);

  const getCurrentSentence = useCallback(() => {
    return getCurrentParagraph()[pointer.sentence];
  }, [getCurrentParagraph, pointer.sentence]);

  const getCurrentWord = useCallback(() => {
    return getCurrentSentence()[pointer.word];
  }, [getCurrentSentence, pointer.word]);

  const getNextPointer = useCallback((): Pointer => {
    if (pointer.word < getCurrentSentence().length - 1) {
      return {
        ...pointer,
        word: pointer.word + 1,
      };
    }
    if (pointer.sentence < getCurrentParagraph().length - 1) {
      return {
        ...pointer,
        word: 0,
        sentence: pointer.sentence + 1,
      };
    }
    if (pointer.paragraph < textParagraphs.length - 1) {
      return {
        paragraph: pointer.paragraph + 1,
        word: 0,
        sentence: 0,
      };
    }

    return {
      paragraph: 0,
      sentence: 0,
      word: 0,
    };
  }, [getCurrentParagraph, getCurrentSentence, pointer, textParagraphs.length]);

  const rewindParagraph = useCallback(() => {
    setIsPlaying(false);
    updatePointer((currentPointer) => {
      if (currentPointer.paragraph > 0) {
        if (currentPointer.sentence === 0 && currentPointer.word === 0) {
          return {
            paragraph: currentPointer.paragraph - 1,
            sentence: 0,
            word: 0,
          };
        }
        return {
          paragraph: currentPointer.paragraph,
          sentence: 0,
          word: 0,
        };
      }
      return {
        paragraph: 0,
        sentence: 0,
        word: 0,
      };
    });
  }, []);

  const rewindSentence = useCallback(() => {
    setIsPlaying(false);
    updatePointer((currentPointer) => {
      if (currentPointer.sentence > 0) {
        if (currentPointer.word === 0) {
          return {
            ...currentPointer,
            sentence: currentPointer.sentence - 1,
            word: 0,
          };
        }
        return {
          ...currentPointer,
          word: 0,
        };
      }
      if (currentPointer.paragraph > 0) {
        const nextParagraphPointer = currentPointer.paragraph - 1;
        const nextParagraph = textParagraphs[nextParagraphPointer];
        return {
          ...currentPointer,
          paragraph: currentPointer.paragraph - 1,
          sentence: nextParagraph.length - 1,
        };
      }
      return {
        paragraph: 0,
        sentence: 0,
        word: 0,
      };
    });
  }, [textParagraphs]);

  const skipParagraph = useCallback(() => {
    updatePointer((currentPointer) => {
      const lastParagraphPointer = textParagraphs.length - 1;

      if (currentPointer.paragraph === lastParagraphPointer) {
        return currentPointer;
      }

      return {
        paragraph: currentPointer.paragraph + 1,
        sentence: 0,
        word: 0,
      };
    });
  }, [textParagraphs.length]);

  const togglePlaying = useCallback(() => {
    setIsPlaying((currentIsPlaying) => !currentIsPlaying);
  }, []);

  useEffect(() => {
    if (isPlaying) {
      const isLastWordOfSentence = getIsLastWordOfSentence(
        getCurrentSentence(),
        pointer.word
      );
      const isLastSentenceOfParagaph = getIsLastSentenceOfParagaph(
        getCurrentParagraph(),
        pointer.sentence
      );

      const delay =
        convertWordsPerMinuteToDelay(wordsPerMinute) *
        (isLastWordOfSentence && isLastSentenceOfParagaph
          ? lastSentenceOfParagaphDelayMultiplier
          : isLastWordOfSentence
          ? lastWordOfSentenceDelayMultiplier
          : 1);

      const timeout = setInterval(() => {
        updatePointer(getNextPointer());
      }, delay);

      return () => {
        clearInterval(timeout);
      };
    }
  }, [
    getCurrentParagraph,
    getCurrentSentence,
    getNextPointer,
    isPlaying,
    pointer.sentence,
    pointer.word,
    wordsPerMinute,
  ]);

  return (
    <>
      <Word word={getCurrentWord()} />
      <div className={styles.wordsPerMinutesSliderWrapper}>
        <Slider
          min={120}
          step={10}
          max={1000}
          orientation="vertical"
          value={wordsPerMinute}
          onChange={(event, newValue) => {
            setWordsPerMinute(newValue as number);
          }}
          aria-labelledby="vertical-slider"
        />
      </div>

      <BottomBar
        isPlaying={isPlaying}
        rewindParagraph={rewindParagraph}
        rewindSentence={rewindSentence}
        skipParagraph={skipParagraph}
        togglePlaying={togglePlaying}
      ></BottomBar>
    </>
  );
};
