import styles from "./word.module.css";

interface WordProps {
  word?: string;
}
export const Word = (props: WordProps) => {
  return (
    <div className={styles.wordbox}>
      <div className={styles.word}>{props.word}</div>
    </div>
  );
};
