import {
  AppBar,
  Button,
  IconButton,
  TextField,
  Toolbar,
  Typography,
} from "@material-ui/core";
import React, { useCallback, useEffect, useState } from "react";
import { Word } from "./Word";
import MenuIcon from "@material-ui/icons/Menu";

export const AddTextPage = () => {
  return (
    <div>
      <TextField
        id="standard-textarea"
        label="Multiline Placeholder"
        placeholder="Placeholder"
        multiline
      />
    </div>
  );
};
